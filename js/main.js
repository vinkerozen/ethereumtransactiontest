/**
 * @author : vincent ollivier
 * @date : jan 2022
 * @version : 1.0
 *
 */

const CHAIN             = { name:'Ganache Local', chainId: 5777, url:'HTTP://127.0.0.1:8545' }; // connect to local Ganache instance
const ACCOUNT_MONEY_PRK = '0x6379203b0407e58a6b585160ce27c88d9b512c4f8c8b9a5592ccc8ea95770ab0'; // one private key from my Ganache accounts


/**
 * Chain class
 */
class Chain {

  constructor(chainToConnect) {
    this.connect(chainToConnect);
  }

  connect(c)
  {
    globalThis.web3 = new Web3(c.url);
  }
}


/**
 * Tersting class
 */
class TestTransaction {

  constructor() {
    this.initUI();
    this.initTest();
  }

  initUI()
  {
    this.form = [];
    this.form.chainConnection   = $("#chainConnection");
    this.form.testButton        = $("#testButton");
    this.form.transactionResult = $("#transactionResult");
    this.form.inputRawTx        = $("#inputRawTx");
    this.form.buttonRawTx       = $("#buttonRawTx");

    this.form.chainConnection.text("Connected to : "+ CHAIN.name + " @ " + CHAIN.url + " / id:"+ CHAIN.chainId);
  }

  initTest()
  {
    const resultText = this.form.transactionResult;

    this.form.testButton.click(function(e) {
      e.preventDefault();
      e.stopPropagation();

      // minimalist transaction description (some missing fields will be auto-completed by web3.js)
      const jsonTx = {
        'gas'  : 30000,
        'gasPrice' : 0x09184e72a000,
        'value': 0, // value to transfer
        'data' : '0x00112233445566778899aabbccddeeff', // payload encoded (or metadata)
        'to'   : '0x4ba6ec46f0cebfd805cc521d84bb6df4af182df8', // destination address (Some Ganache random address)
      };

      web3.eth.accounts.signTransaction(jsonTx, ACCOUNT_MONEY_PRK).then((signedtx) => {
        console.log(signedtx);
        web3.eth.sendSignedTransaction(signedtx.rawTransaction).then(value => {
          resultText.text("Transaction successfully proceed !");
          console.log(value);
        }, (reason) =>  {
          resultText.text("Transaction failed : "+reason);
          console.error(reason);
        });
      }, (reason) =>  {
        resultText.text("Error :"+reason);
        console.error(reason);
      });
    });



    this.form.buttonRawTx.click(function(e) {
      e.preventDefault();
      e.stopPropagation();

      let SignedRawTransaction = this.form.inputRawTx.value;

      web3.eth.sendSignedTransaction(SignedRawTransaction).then(value => {
        resultText.text("Transaction successfully proceed !");
        console.log(value);
      }, (reason) =>  {
        resultText.text("Transaction failed : "+reason);
        console.error(reason);
      });

    });

  }

}

let chain = new Chain(CHAIN);
let testTransaction = new TestTransaction();
